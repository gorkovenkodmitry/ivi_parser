# coding: utf-8
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from codecs import getwriter
import json
import requests
from requests.adapters import HTTPAdapter
from signal import signal, SIGINT, SIGTERM
from sys import stdout, exit, exc_info, stderr
import threading
import Queue

BASE_URL = 'http://www.ivi.ru/'
TIMEOUT = 75
MAX_RETRIES = 10


class Mult(object):

    def __init__(self, data):
        self._session = requests.Session()
        self._session.mount(BASE_URL, HTTPAdapter(max_retries=MAX_RETRIES))
        self._collections = {}
        self._data = data
        self._load_description()
        self._load_person()

    @property
    def url(self):
        return BASE_URL+'watch/'+str(self.get_hru())

    def _get_page(self, url):
        response = self._session.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        return soup.find('div', attrs={'class': 'content-main'})

    @staticmethod
    def _clean(value):
        return value.replace('\n', '').strip()
    
    def _load_description(self):
        url = self.url
        if self._data.get('hru') is None:
            url += '/description'
        page = self._get_page(url)

        try:
            self._OriginalTitle = page.find('div', attrs={'class': 'title-block'}).find('small').text
        except AttributeError:
            self._OriginalTitle = None

        try:
            dates = page.find('span', attrs={'itemprop': 'datePublished'})
            if dates is None:
                dates = page.find('a', attrs={'itemprop': 'datePublished'})
            dates = dates.text.split('-')
            self._InitDate = dates[0]
            self._EndDate = dates[-1] if len(dates) > 1 else None
        except AttributeError:
            self._InitDate = None
            self._EndDate = None

        try:
            self._country = self._clean(page.find('a', attrs={'itemprop': 'productionCountry'}).text)
        except AttributeError:
            self._country = None

        try:
            self._facts = []
            facts = page.find('ul', attrs={'class': 'ticks loose js-expand-block'})
            for li in facts.findAll('li'):
                self._facts.append({'value': self._clean(li.text)})
        except AttributeError:
            self._facts = None

        try:
            self._description = self._clean(page.find('div', attrs={'class': 'description', 'itemprop': 'description'}).text)
        except AttributeError:
            self._description = None

        try:
            self._types = []
            for a in page.findAll('a', attrs={'itemprop': 'genre'}):
                self._types.append(self._clean(a.text))
        except AttributeError:
            self._types = []

        try:
            self._set_collections(page.find('ul', attrs={'class': 'related-links'}).findAll('li'))
            self._categs = self._collections.keys()
        except AttributeError:
            self._categs = None

        try:
            self._rels = []
            for li in page.find('section', attrs={'class': 'posters-block js-related-block'}).find('ul', attrs={'class': 'gallery posters-large js-popups-block'}).findAll('li'):
                self._rels.append(self._get_rel_id(BASE_URL[:-1]+li.find('a', attrs={'class': 'item-content-wrapper'}).get('href')))
        except AttributeError:
            self._rels = None

        try:
            self._contentRating = self._clean(page.find('span', attrs={'itemprop': 'contentRating'}).text)
        except AttributeError:
            self._contentRating = u'Для всей семьи'

        try:
            self._budget = []
            for dd in page.find('dl', attrs={'class': 'coupled'}).findAll('dd'):
                self.budget.append({'value': self._clean(dd.text)})
        except AttributeError:
            self._budget = None

        try:
            self._duration = self._clean(page.find('time', attrs={'itemprop': 'duration'}).text.replace(u'мин.', ''))
        except AttributeError:
            self._duration = None

        self._ratingValue = None
        self._bestRating = None
        try:
            self._ratingValue = self._clean(page.find('span', attrs={'itemprop': 'ratingValue'}).text)
            self._bestRating = self._clean(page.find('meta', attrs={'itemprop': 'bestRating'}).get('content'))
        except AttributeError:
            pass

    def _get_rel_id(self, href):
        return '#ext_ivi_animation_%s' % href.split('/')[-1]

    def _get_collection_id(self, href):
        return '#ext_ivi_%s' % '_'.join(href[1:].split('/'))

    def _set_collections(self, rels):
        for li in rels:
            a = li.find('a')
            try:
                self._collections[self._get_collection_id(a.get('href'))] = {
                    'title': self._clean(a.text),
                    'url': BASE_URL[:-1]+a.get('href'),
                    'ontoid': self._get_collection_id(a.get('href'))
                }
            except AttributeError:
                pass

    def _load_person(self):
        page = self._get_page(self.url+'/person')
        directors = page.find('ul', attrs={'class': 'roomy-list', 'itemprop': 'director'})
        self._directors = []
        if directors is None:
            return
        for li in directors.findAll('li'):
            self._directors.append({
                "Role": "Director@on",
                "value": self._clean(li.find('span').text if li.find('span') else li.text),
                "langua": "ru"
            })


    def get_hru(self):
        return self._data.get('hru', self._data['id'])

    @property
    def ontoid(self):
        return 'ext_ivi_animation_%s' % self.get_hru()

    @property
    def ids(self):
        return {
            "type": "url",
            "value": "http://www.ivi.ru/watch/%s" % self.get_hru(),
            "langua": "ru"
        }

    @property
    def Title(self):
        return {
            "value": "%s" % self._data.get('title'),
            "langua": "ru"
        }

    @property
    def img_url(self):
        if hasattr(self, '_img_url'):
            return self._img_url
        try:
            self._img_url = self._data['poster_originals'][0]['path']
        except (KeyError, IndexError):
            self._img_url = None
        return self._img_url
    
    def result(self):
        data = {
            'ontoid': self.ontoid,
            'ids': [self.ids],
            "Title": [self.Title],
        }
        if self._OriginalTitle:
            data['OriginalTitle'] = [{'value': self._OriginalTitle}]
        if self.img_url:
            data['Image'] = [{
                'src': [{
                    'url': self.url
                }],
                'value': self.img_url,
                'url': self.img_url
            }]
        if self._InitDate:
            data['InitDate'] = [{'value': self._InitDate}]
        if self._EndDate:
            data['EndDate'] = [{'value': self._EndDate}]
        if self._country:
            data['Countries'] = [{'value': self._country}]
        if self._directors:
            data['Participants'] = self._directors
        if self._facts:
            data['InterestingFacts'] = self._facts
        data['isa'] = {}
        if self._description:
            data['isa']['Defin'] = [{'value': self._description}]
        data['isa']['otype'] = [{'value': 'Film'}]
        if u'Сериалы' in self._types:
            data['isa']['otype'].append({'subvalue': 'Series@on'})
        if self._types:
            data['isa']['Type'] = []
            for x in self._types:
                data['isa']['Type'].append({'value': x})
        data['isa']['tags'] = [{'value': 'Animation@on'}]

        if self._categs:
            data['categs'] = [{'value': '[[%s]]'%x for x in self._categs}]
        if self._rels:
            data['rels'] = {'Related': [{'value': '[[%s]]'%x for x in self._rels}]}

        data['params'] = {'AgeLimit': [{'value': self._contentRating}]}
        if self._budget:
            data['params']['Budget'] = self._budget
        if self._duration:
            data['params']['Duration'] = [{'value': self._duration}]

        if self._ratingValue:
            data['params']['Rating'] = [{'value': self._ratingValue}]
            if self._bestRating:
                data['params']['Rating'][0]['max_value'] = self._ratingValue
        return data, self._collections


class Client(object):
    THREADS = 32
    PAGE_COUNT = 100
    BASE_API_URL = 'https://api.ivi.ru/mobileapi/catalogue/v5/?sort=pop&fields=id,title,fake,available_in_countries,hru,poster_originals,content_paid_types,compilation_hru,kind,additional_data,restrict&category=17&app_version=870'
    BASE_HD_URL = 'https://api.ivi.ru/mobileapi/collection/catalog/v5/?fields=id,title,fake,available_in_countries,hru,poster_originals,content_paid_types,compilation_hru,kind,additional_data,restrict&sort=relevance&fake=0&withpreorderable=0&id=1525&app_version=870'

    def __init__(self):
        self._session = requests.Session()
        self._session.mount(BASE_URL, HTTPAdapter(max_retries=MAX_RETRIES))
        self._session.get(BASE_URL, timeout=TIMEOUT)
        self._queue = {}
        self._result = []
        self._collections = {}
        self._result_collections = []
        ind = 0
        while True:
            params = {'from': (ind*self.PAGE_COUNT), 'to': (ind+1)*self.PAGE_COUNT-1}
            response = self._session.get(self.BASE_API_URL, params=params, timeout=TIMEOUT)
            x = json.loads(response.content)
            for mult in x['result']:
                self._queue[mult['id']] = mult
            if len(x['result']) == 0:
                break
            ind += 1
        ind = 0
        while True:
            params = {'from': (ind*self.PAGE_COUNT), 'to': (ind+1)*self.PAGE_COUNT-1}
            response = self._session.get(self.BASE_HD_URL, params=params, timeout=TIMEOUT)
            x = json.loads(response.content)
            for mult in x['result']:
                try:
                    del self._queue[mult['id']]
                except KeyError:
                    pass
            if len(x['result']) == 0:
                break
            ind += 1

    def _set_collections(self):
        for key, item in self._collections.items():
            self._result_collections.append({
                'ontoid': '%s'%key[1:],
                'ids': [{
                    'type': "url",
                    'value': item['url'],
                    'langua': "ru"
                }],
                'Title': [{
                    'value': item['title'],
                    'langua': 'ru'
                }],
                'isa':{
                    'otype': [{
                    'value': 'List'
                }],
                'tags': [{
                    'value': 'Animation@on'
                }]
                },
                'Assoc': {
                    'ru':[{'value': x} for x in item['mults']]
                }
            })


    def process(self):
        threads = []
        work_q = Queue.Queue()
        raw_data = Queue.Queue()
        for key, item in self._queue.items():
            work_q.put(item)


        def mult_process(work_q, raw_data):
            while True:
                if work_q.empty():
                    break
                mult = work_q.get()
                res, collections = Mult(mult).result()
                raw_data.put((res, collections))

        for x in range(self.THREADS):
            threads.append(threading.Thread(target=mult_process, args=(work_q, raw_data)))
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        while True:
            if raw_data.empty():
                break
            res, collections = raw_data.get_nowait()
            for key, item in collections.items():
                if key not in self._collections:
                    self._collections[key] = item
                    self._collections[key]['mults'] = set()
                self._collections[key]['mults'].add('[[#%s]]'%res['ontoid'])
            self._result.append(res)
        self._set_collections()


    @property
    def result_collections(self):
        return self._result_collections

    @property
    def result(self):
        return self._result

def read_key():
    argp = ArgumentParser()
    argp.add_argument('--type', type=str, required=True, help="Type")
    args = argp.parse_args()
    return args.type


def main():
    sout = getwriter("utf8")(stdout) 
    serr = getwriter("utf8")(stderr)
    client = Client()
    dump_type = read_key()
    try:
        client.process()
        if dump_type == 'mult':
            for res in client.result:
                sout.write(json.dumps(res, ensure_ascii=False) + "\n")
        elif dump_type == 'categs':
            for res in client.result_collections:
                sout.write(json.dumps(res, ensure_ascii=False) + "\n")
    except Exception as e:
        exc_traceback = exc_info()[2]
        filename = line = None
        while exc_traceback is not None:
            f = exc_traceback.tb_frame
            line = exc_traceback.tb_lineno
            filename = f.f_code.co_filename
            exc_traceback = exc_traceback.tb_next
        serr.write(json.dumps({
            'error': True,
            'details': {
                'message': str(e),
                'file': filename,
                'line': line
            }
        }, ensure_ascii=False) + "\n")


if __name__ == "__main__": 
    def signal_handler(signal, frame): 
        exit(0)
    signal(SIGINT, signal_handler)
    signal(SIGTERM, signal_handler)
    main() 
